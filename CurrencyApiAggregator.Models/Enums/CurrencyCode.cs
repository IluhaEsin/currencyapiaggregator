﻿namespace CurrencyApiAggregator.Models.Enums
{
    public enum CurrencyCode
    {
        Btc,
        Eth,
        Xrp,
        Aud
    }
}
