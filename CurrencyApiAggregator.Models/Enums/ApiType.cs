﻿namespace CurrencyApiAggregator.Models.Enums
{
    public enum ApiType
    {
        IndependentReserve,
        BtcMarkets
    }
}
