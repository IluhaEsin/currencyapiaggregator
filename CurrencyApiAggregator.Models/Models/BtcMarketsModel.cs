﻿using System;
using Newtonsoft.Json;

namespace CurrencyApiAggregator.Models.Models
{
    public class BtcMarketsModel : MarketModel
    {
        [JsonProperty("lastPrice")]
        public override decimal Price { get; set; }

        [JsonProperty("volume24h")]
        public override decimal DayVol { get; set; }

        [JsonProperty("errorMessage")]
        public override string ErrorMessage { get; set; }

        [JsonProperty("timestamp")]
        public override string TimeStamp { get; set; }

        public override DateTime CreateDateTimeUtc
        {
            get
            {
                DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                return dtDateTime.AddSeconds(Convert.ToInt32(TimeStamp)).ToUniversalTime();
            }
        }
    }
}
