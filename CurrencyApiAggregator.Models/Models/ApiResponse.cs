﻿namespace CurrencyApiAggregator.Models.Models
{
    public class ApiResponse
    {
        public bool IsSuccess { get; }

        public string ErrorMessage { get; }

        public MarketModel MarketModel { get; }

        private ApiResponse(bool isSuccess, string errorMessage)
        {
            IsSuccess = isSuccess;
            ErrorMessage = errorMessage;
            MarketModel = null;
        }

        private ApiResponse(bool isSuccess, MarketModel marketModel)
        {
            IsSuccess = isSuccess;
            ErrorMessage = string.Empty;
            MarketModel = marketModel;
        }

        public static ApiResponse Error(string apiName, string message)
        {
            return new ApiResponse(false, $"{apiName}. {message}");
        }

        public static ApiResponse Success(MarketModel marketModel)
        {
            return new ApiResponse(true, marketModel);
        }
    }
}