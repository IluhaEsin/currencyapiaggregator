﻿using System.Collections.Generic;

namespace CurrencyApiAggregator.Models.Models
{
    public class ApiSettings
    {
        public IEnumerable<ApiDataModel> ApiList { get; set; }
        public int CachedDataLifetimeInSeconds { get; set; }
        public int RequestTimeOutInSeconds { get; set; }
    }
}
