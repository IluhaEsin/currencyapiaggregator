﻿using System;

namespace CurrencyApiAggregator.Models.Models
{
    public abstract class MarketModel
    {
        public abstract string ErrorMessage { get; set; }

        public abstract string TimeStamp { get; set; }

        public abstract DateTime CreateDateTimeUtc { get; }

        public abstract decimal Price { get; set; }

        public abstract decimal DayVol { get; set; }

        public bool Success => string.IsNullOrEmpty(ErrorMessage);

        public override string ToString()
        {
            if (Success)
            {
                return base.ToString();
            }
            else
            {
                return ErrorMessage;
            }
        }
    }
}