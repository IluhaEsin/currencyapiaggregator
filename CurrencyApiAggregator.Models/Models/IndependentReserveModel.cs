﻿using System;
using Newtonsoft.Json;

namespace CurrencyApiAggregator.Models.Models
{
    public class IndependentReserveModel : MarketModel
    {
        [JsonProperty("LastPrice")]
        public override decimal Price { get; set; }

        [JsonProperty("DayVolumeXbt")]
        public override decimal DayVol { get; set; }

        [JsonProperty("Message")]
        public override string ErrorMessage { get; set; }

        [JsonProperty("CreatedTimestampUtc")]
        public override string TimeStamp { get; set; }

        public override DateTime CreateDateTimeUtc => DateTime.Parse(TimeStamp);
    }
}
