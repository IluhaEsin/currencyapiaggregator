﻿using CurrencyApiAggregator.Models.Enums;

namespace CurrencyApiAggregator.Models.Models
{
    public class RequestCurrenciesData
    {
        public CurrencyCode CryptoCurrencyCode { get; }
        public CurrencyCode CurrencyCode { get; }

        public RequestCurrenciesData(CurrencyCode cryptoCurrencyCode, CurrencyCode currencyCode)
        {
            CryptoCurrencyCode = cryptoCurrencyCode;
            CurrencyCode = currencyCode;
        }
    }
}
