﻿namespace CurrencyApiAggregator.Models.Models
{
    public class ApiDataModel
    {
        public string Name { get; set; }
        public string UriFormat { get; set; }
    }
}
