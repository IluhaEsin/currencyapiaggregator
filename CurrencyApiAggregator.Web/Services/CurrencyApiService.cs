﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CurrencyApiAggregator.Models.Enums;
using CurrencyApiAggregator.Models.Models;
using CurrencyApiAggregator.Web.Models;
using CurrencyApiAggregator.Web.Services.Contracts;

namespace CurrencyApiAggregator.Web.Services
{
    public class CurrencyApiService : ICurrencyApiService
    {
        private readonly IMarketDataCacheService _marketDataCacheService;
        private readonly ICollection<CurrencyCode> _cryptoCurrencyCodes;
        private const CurrencyCode BaseCurrencyCode = CurrencyCode.Aud;

        public CurrencyApiService(IMarketDataCacheService marketDataCacheService)
        {
            if(marketDataCacheService == null)
                throw new ArgumentNullException(nameof(marketDataCacheService));

            _marketDataCacheService = marketDataCacheService;
            _cryptoCurrencyCodes = new[] {CurrencyCode.Btc, CurrencyCode.Eth, CurrencyCode.Xrp};
        }

        /// <summary>
        /// Created collection of CurrencySummary objects, containing calculated info from APIs
        /// </summary>
        public async Task<IEnumerable<CurrencySummary>> CreateSummary()
        {
            var summaries = new List<CurrencySummary>();
            foreach (var code in _cryptoCurrencyCodes)
            {
                var marketDataResponses = new List<ApiResponse>();
                foreach (ApiType apiType in Enum.GetValues(typeof(ApiType)))
                {
                    var requestData = new MarketRequestData(apiType, code, BaseCurrencyCode);
                    var response = await _marketDataCacheService.GetCachedOrAddMarketDataAsync(requestData);
                    marketDataResponses.Add(response);
                }
                summaries.Add(new CurrencySummary(code, marketDataResponses));
            }
            
            return summaries;
        }
    }
}
