﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CurrencyApiAggregator.Web.Models;

namespace CurrencyApiAggregator.Web.Services.Contracts
{
    public interface ICurrencyApiService
    {
        Task<IEnumerable<CurrencySummary>> CreateSummary();
    }
}