﻿using System.Threading.Tasks;
using CurrencyApiAggregator.Models.Models;
using CurrencyApiAggregator.Web.Models;

namespace CurrencyApiAggregator.Web.Services.Contracts
{
    public interface IMarketDataCacheService
    {
        Task<ApiResponse> GetCachedOrAddMarketDataAsync(MarketRequestData marketRequestData);
    }
}