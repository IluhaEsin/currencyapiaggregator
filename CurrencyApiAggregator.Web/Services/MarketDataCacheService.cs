﻿using System;
using System.Threading.Tasks;
using CurrencyApiAggregator.Client.Factories;
using CurrencyApiAggregator.Models.Models;
using CurrencyApiAggregator.Web.Models;
using CurrencyApiAggregator.Web.Services.Contracts;
using Microsoft.Extensions.Caching.Memory;

namespace CurrencyApiAggregator.Web.Services
{
    public class MarketDataCacheService : IMarketDataCacheService
    {
        private readonly IMemoryCache _cache;
        private readonly IApiClientFactory _apiClientFactory;
        private readonly TimeSpan _cachedDataLifetime;

        public MarketDataCacheService(IMemoryCache cache, IApiClientFactory apiClientFactory, TimeSpan cachedDataLifetime)
        {
            _cache = cache;
            _apiClientFactory = apiClientFactory;
            _cachedDataLifetime = cachedDataLifetime;
        }

        /// <summary>
        /// Gets cached or create result of api request. Caches each request result by key, representing api type and request parameters
        /// </summary>
        public async Task<ApiResponse> GetCachedOrAddMarketDataAsync(MarketRequestData marketRequestData)
        {
            return await _cache.GetOrCreateAsync(marketRequestData, async entry =>
            {
                entry.AbsoluteExpirationRelativeToNow = _cachedDataLifetime;
                return await GetMarketDataAsync(marketRequestData);
            }); ;
        }

        private async Task<ApiResponse> GetMarketDataAsync(MarketRequestData marketRequestData)
        {
            using (var apiClient = _apiClientFactory.Create(marketRequestData.ApiType))
            {
                return await apiClient.GetMarketDataAsync(new RequestCurrenciesData(marketRequestData.CryptoCurrencyCode, marketRequestData.CurrencyCode));
            }
        }
    }
}