﻿using System;
using CurrencyApiAggregator.Models.Enums;

namespace CurrencyApiAggregator.Web.Models
{
    public class MarketRequestData : IEquatable<MarketRequestData>
    {
        public ApiType ApiType { get; }
        public CurrencyCode CryptoCurrencyCode { get; }
        public CurrencyCode CurrencyCode { get; }

        public MarketRequestData(ApiType apiType, CurrencyCode cryptoCurrencyCode, CurrencyCode currencyCode)
        {
            ApiType = apiType;
            CryptoCurrencyCode = cryptoCurrencyCode;
            CurrencyCode = currencyCode;
        }

        public bool Equals(MarketRequestData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return ApiType == other.ApiType && CryptoCurrencyCode == other.CryptoCurrencyCode && CurrencyCode == other.CurrencyCode;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((MarketRequestData) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (int) ApiType;
                hashCode = (hashCode * 397) ^ (int) CryptoCurrencyCode;
                hashCode = (hashCode * 397) ^ (int) CurrencyCode;
                return hashCode;
            }
        }
    }
}