﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using CurrencyApiAggregator.Client.Client;
using CurrencyApiAggregator.Models.Enums;
using CurrencyApiAggregator.Models.Models;

namespace CurrencyApiAggregator.Web.Models
{
    public class CurrencySummary
    {
        private readonly CurrencyCode _currencyCode;
        private IEnumerable<ApiResponse> _apiResponses;

        [Display(Name = "Currency")]
        public string Currency
        {
            get
            {
                var value = _currencyCode.ToString().ToUpper();
                if (!IsSuccess)
                    value =
                        $"{value}{Environment.NewLine}Errors: {string.Join(Environment.NewLine, _apiResponses.Select(ar => ar.ErrorMessage))}";
                return value;
            }
        }

        [Display(Name = "Price (Last Price)")]
        public string AveragePrice
        {
            get
            {
                var value = decimal.Zero;
                if (IsSuccess)
                    value = _apiResponses.Average(m => m.MarketModel.Price);

                return value.ToString("C");
            }
        }

        [Display(Name = "24h Vol")]
        public string TotalPrice
        {
            get
            {
                var value = decimal.Zero;
                if (IsSuccess)
                    value = Math.Round(_apiResponses.Sum(m => m.MarketModel.DayVol), 2);

                return value.ToString("G", CultureInfo.InvariantCulture);
            }
        }

        public CurrencySummary(CurrencyCode currencyCode, List<ApiResponse> apiResponses)
        {
            if(Enum.GetNames(typeof(ApiType)).Length != apiResponses.Count())
                throw new NotSupportedException("Incorrect count of calculating data");

            _currencyCode = currencyCode;
            _apiResponses = apiResponses;
        }

        private bool IsSuccess => _apiResponses.All(ar => ar.IsSuccess);
    }
}
