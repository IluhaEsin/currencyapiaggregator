﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CurrencyApiAggregator.Web.Models;
using CurrencyApiAggregator.Web.Services.Contracts;
using Microsoft.Extensions.Configuration;

namespace CurrencyApiAggregator.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICurrencyApiService _currencyApiService;
        private readonly IConfiguration _configuration;

        public HomeController(ICurrencyApiService currencyApiService, IConfiguration configuration)
        {
            _currencyApiService = currencyApiService;
            _configuration = configuration;
        }

        public IActionResult Index()
        {
            var interval = _configuration.GetValue<int>("AggregationTableRefreshIntervalInSeconds");
            ViewBag.RefreshInterval = TimeSpan.FromSeconds(interval).TotalMilliseconds;
            return View();
        }

        public async Task<IActionResult> GetMarketData()
        {
            var summary = await _currencyApiService.CreateSummary();

            return Ok(summary);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
