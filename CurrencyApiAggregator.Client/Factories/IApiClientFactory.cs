﻿using CurrencyApiAggregator.Client.Client;
using CurrencyApiAggregator.Models.Enums;

namespace CurrencyApiAggregator.Client.Factories
{
    public interface IApiClientFactory
    {
        IApiClient Create(ApiType apiType);
    }
}