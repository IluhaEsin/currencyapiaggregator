﻿using System;
using System.Linq;
using System.Net.Http;
using CurrencyApiAggregator.Client.Client;
using CurrencyApiAggregator.Client.Utils;
using CurrencyApiAggregator.Models.Enums;
using CurrencyApiAggregator.Models.Models;

namespace CurrencyApiAggregator.Client.Factories
{
    public class ApiClientFactory : IApiClientFactory
    {
        private readonly ApiSettings _apiSettings;
        
        public ApiClientFactory(ApiSettings apiSettings)
        {
            if (apiSettings == null)
                throw new ArgumentNullException(nameof(apiSettings));

            _apiSettings = apiSettings;
        }


        /// <summary>
        /// Creates an api client object for specific api type
        /// </summary>
        /// <returns>
        /// Api client to make http request to api
        /// </returns>
        /// <param name="apiType">type of api</param>
        public IApiClient Create(ApiType apiType)
        {
            string uriFormat = _apiSettings.ApiList.FirstOrDefault(adm => adm.Name == apiType.ToString())?.UriFormat;
            var httpClient = new HttpClient
            {
                Timeout = TimeSpan.FromSeconds(_apiSettings.RequestTimeOutInSeconds)
            };
            switch (apiType)
            {
                case ApiType.IndependentReserve:
                    var irUriCreator = new ApiUriCreator(
                        new IndependentReserveCurrencyCodeConverter(),
                        uriFormat
                    );
                    var irApiResponseManager = new ApiResponseManager<IndependentReserveModel>(apiType.ToString());
                    return new ApiClient(irUriCreator, irApiResponseManager, httpClient);
                case ApiType.BtcMarkets:
                    var bmUriCreator = new ApiUriCreator(
                        new BtcMarketsCurrencyCodeConverter(),
                        uriFormat
                    );
                    var bmApiResponseManager = new ApiResponseManager<BtcMarketsModel>(apiType.ToString());
                    return new ApiClient(bmUriCreator, bmApiResponseManager, httpClient);

            }

            throw new NotSupportedException($"Impossible to create api client by api type {apiType.ToString()}");
        }
    }
}
