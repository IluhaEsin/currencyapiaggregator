﻿using CurrencyApiAggregator.Client.Utils.Contracts;
using CurrencyApiAggregator.Models.Enums;

namespace CurrencyApiAggregator.Client.Utils
{
    public class BtcMarketsCurrencyCodeConverter : ICurrencyCodeConverter
    {
        public string Convert(CurrencyCode code)
        {
            return code.ToString().ToUpper();
        }
    }
}