﻿using CurrencyApiAggregator.Client.Utils.Contracts;
using CurrencyApiAggregator.Models.Enums;

namespace CurrencyApiAggregator.Client.Utils
{
    public class IndependentReserveCurrencyCodeConverter : ICurrencyCodeConverter
    {
        public string Convert(CurrencyCode code)
        {
            switch (code)
            {
                case CurrencyCode.Btc: return "xbt";
                default: return code.ToString().ToLower();
            }
        }
    }
}