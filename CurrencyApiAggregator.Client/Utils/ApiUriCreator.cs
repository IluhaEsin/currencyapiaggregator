﻿using System;
using CurrencyApiAggregator.Client.Utils.Contracts;
using CurrencyApiAggregator.Models.Enums;

namespace CurrencyApiAggregator.Client.Utils
{
    public class ApiUriCreator : IApiUriCreator
    {
        private readonly ICurrencyCodeConverter _codeConverter;
        private readonly string _uriFormat;

        public ApiUriCreator(ICurrencyCodeConverter codeConverter, string uriFormat)
        {
            if(codeConverter == null)
                throw new ArgumentNullException(nameof(codeConverter));
            if(uriFormat == null)
                throw new ArgumentNullException(nameof(uriFormat));

            _codeConverter = codeConverter;
            _uriFormat = uriFormat;
        }

        /// <summary>
        /// Created uri from formatted string and currency codes
        /// </summary>
        /// <returns>
        /// Uri object for addressing http request
        /// </returns>
        /// <param name="cryptoCurrencyCode">crypto currency code i.e. btc, eth</param>
        /// <param name="currencyCode">regular currency code</param>
        public Uri Create(CurrencyCode cryptoCurrencyCode, CurrencyCode currencyCode)
        {
            return new Uri(string.Format(_uriFormat,
                _codeConverter.Convert(cryptoCurrencyCode),
                _codeConverter.Convert(currencyCode))); 
        }
    }
}