﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using CurrencyApiAggregator.Client.Utils.Contracts;
using CurrencyApiAggregator.Models.Models;
using Newtonsoft.Json;

namespace CurrencyApiAggregator.Client.Utils
{
    public class ApiResponseManager<TDataModel> : IApiResponseManager
        where TDataModel : MarketModel
    {
        private readonly string _apiName;

        public ApiResponseManager(string apiName)
        {
            _apiName = apiName;
        }

        public string ApiName => _apiName;

        /// <summary>
        /// Converts string response from api to ApiResponse object
        /// </summary>
        /// <returns>
        /// ApiResponse object with retrieved data or error message
        /// </returns>
        /// <param name="responseData">string representation of data retrieved from http response</param>
        public ApiResponse CreateResponse(string responseData)
        {
            DateTime a = DateTime.Now.ToUniversalTime();
            var deserializedObject = JsonConvert.DeserializeObject<TDataModel>(responseData);

            if (deserializedObject.Success)
            {
                return ApiResponse.Success(deserializedObject);
            }

            return ApiResponse.Error(_apiName, deserializedObject.ToString());
        }
    }
}