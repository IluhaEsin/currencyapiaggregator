﻿using System.Net.Http;
using System.Threading.Tasks;
using CurrencyApiAggregator.Models.Models;

namespace CurrencyApiAggregator.Client.Utils.Contracts
{
    public interface IApiResponseManager
    {
        string ApiName { get; }

        ApiResponse CreateResponse(string responseData);
    }
}