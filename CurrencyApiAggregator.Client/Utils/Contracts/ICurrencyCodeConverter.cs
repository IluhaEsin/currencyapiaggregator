﻿using CurrencyApiAggregator.Models.Enums;

namespace CurrencyApiAggregator.Client.Utils.Contracts
{
    public interface ICurrencyCodeConverter
    {
        string Convert(CurrencyCode code);
    }
}