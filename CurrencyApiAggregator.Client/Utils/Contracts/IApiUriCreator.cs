﻿using System;
using CurrencyApiAggregator.Models.Enums;

namespace CurrencyApiAggregator.Client.Utils.Contracts
{
    public interface IApiUriCreator
    {
        Uri Create(CurrencyCode cryptoCurrencyCode, CurrencyCode currencyCode);
    }
}
