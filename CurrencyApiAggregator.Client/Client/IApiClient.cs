﻿using System;
using System.Threading.Tasks;
using CurrencyApiAggregator.Models.Models;

namespace CurrencyApiAggregator.Client.Client
{
    public interface IApiClient : IDisposable
    {
        Task<ApiResponse> GetMarketDataAsync(RequestCurrenciesData requestCurrenciesData);
    }
}