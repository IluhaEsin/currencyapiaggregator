﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using CurrencyApiAggregator.Client.Utils.Contracts;
using CurrencyApiAggregator.Models.Models;

namespace CurrencyApiAggregator.Client.Client
{
    public class ApiClient : IApiClient
    {
        private readonly HttpClient _httpClient;
        private readonly IApiUriCreator _apiUriCreator;
        private readonly IApiResponseManager _apiResponseManager;

        private bool _disposed;

        public ApiClient(IApiUriCreator apiUriCreator, IApiResponseManager apiResponseManager, HttpClient httpClient)
        {
            if (apiUriCreator == null)
                throw new ArgumentNullException(nameof(apiUriCreator));
            if (apiResponseManager == null)
                throw new ArgumentNullException(nameof(apiResponseManager));

            _apiUriCreator = apiUriCreator;
            _apiResponseManager = apiResponseManager;
            _httpClient = httpClient;
        }

        /// <summary>
        /// Makes an http request to api
        /// </summary>
        /// <returns>
        /// Response, containing MarketModel with data, result of operation and error message in case of failure
        /// </returns>
        /// <param name="requestCurrenciesData">contains a pair of currencies codes for request</param>
        public async Task<ApiResponse> GetMarketDataAsync(RequestCurrenciesData requestCurrenciesData)
        {
            var requestUrl = _apiUriCreator.Create(
                requestCurrenciesData.CryptoCurrencyCode,
                requestCurrenciesData.CurrencyCode);
            return await GetAsync(requestUrl);
        }

        private async Task<ApiResponse> GetAsync(Uri requestUrl)
        {
            try
            {
                var response = await _httpClient.GetAsync(requestUrl, HttpCompletionOption.ResponseHeadersRead);

                if (!response.IsSuccessStatusCode)
                {
                    return ApiResponse.Error(_apiResponseManager.ApiName, response.StatusCode.ToString());
                }

                var data = await response.Content.ReadAsStringAsync();

                return _apiResponseManager.CreateResponse(data);
            }
            catch (Exception e)
            {
                return ApiResponse.Error(_apiResponseManager.ApiName, e.Message);
            }
        }

        #region DisposablePattern

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _httpClient.Dispose();

            _disposed = true;
        }

        ~ApiClient()
        {
            Dispose(false);
        }

        #endregion
    }
}
