using System;
using System.Threading;
using CurrencyApiAggregator.Models.Enums;
using CurrencyApiAggregator.Web.Models;
using CurrencyApiAggregator.Web.Services.Contracts;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace CurrencyApiAggregator.Tests
{
    public class MarketDataCacheServiceTest
    {
        private IMarketDataCacheService _marketDataCacheService;
        private IServiceProvider _serviceProvider;
        private const int timeIntervalWithinCacheLifeTimeInMilliSeconds = 5000;
        private const int timeIntervalMoreThanCacheLifeTimeInMilliSeconds = 20000;

        [SetUp]
        public void Setup()
        {
            _serviceProvider = TestServiceProviderFactory.Create();

            _marketDataCacheService = _serviceProvider.GetRequiredService<IMarketDataCacheService>();
        }

        [TestCase(ApiType.IndependentReserve, CurrencyCode.Btc)]
        [TestCase(ApiType.IndependentReserve, CurrencyCode.Eth)]
        [TestCase(ApiType.IndependentReserve, CurrencyCode.Xrp)]
        [TestCase(ApiType.BtcMarkets, CurrencyCode.Btc)]
        [TestCase(ApiType.BtcMarkets, CurrencyCode.Eth)]
        [TestCase(ApiType.BtcMarkets, CurrencyCode.Xrp)]
        public void EnsureUsingCachedDataWithinCacheLifeTimePeriod(ApiType apiType, CurrencyCode cryptoCurrencyCode)
        {
            var requestData = new MarketRequestData(apiType, cryptoCurrencyCode, CurrencyCode.Aud);
            var firstResult = _marketDataCacheService.GetCachedOrAddMarketDataAsync(requestData).GetAwaiter().GetResult();
            Thread.Sleep(timeIntervalWithinCacheLifeTimeInMilliSeconds);
            var secondResult = _marketDataCacheService.GetCachedOrAddMarketDataAsync(requestData).GetAwaiter().GetResult();

            firstResult.MarketModel.CreateDateTimeUtc.Should().Be(secondResult.MarketModel.CreateDateTimeUtc);
        }

        [TestCase(ApiType.IndependentReserve, CurrencyCode.Btc)]
        [TestCase(ApiType.IndependentReserve, CurrencyCode.Eth)]
        [TestCase(ApiType.IndependentReserve, CurrencyCode.Xrp)]
        [TestCase(ApiType.BtcMarkets, CurrencyCode.Btc)]
        [TestCase(ApiType.BtcMarkets, CurrencyCode.Eth)]
        [TestCase(ApiType.BtcMarkets, CurrencyCode.Xrp)]
        public void EnsureMakingNewRequestWhenCacheLifeTimePeriodOver(ApiType apiType, CurrencyCode cryptoCurrencyCode)
        {
            var requestData = new MarketRequestData(apiType, cryptoCurrencyCode, CurrencyCode.Aud);
            var firstResult = _marketDataCacheService.GetCachedOrAddMarketDataAsync(requestData).GetAwaiter().GetResult();
            Thread.Sleep(timeIntervalMoreThanCacheLifeTimeInMilliSeconds);
            var secondResult = _marketDataCacheService.GetCachedOrAddMarketDataAsync(requestData).GetAwaiter().GetResult();

            firstResult.MarketModel.CreateDateTimeUtc.Should().BeBefore(secondResult.MarketModel.CreateDateTimeUtc);
        }
    }
}