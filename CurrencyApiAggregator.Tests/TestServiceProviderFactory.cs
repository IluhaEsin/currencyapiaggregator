using System;
using CurrencyApiAggregator.Client.Factories;
using CurrencyApiAggregator.Models.Models;
using CurrencyApiAggregator.Web.Services;
using CurrencyApiAggregator.Web.Services.Contracts;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace CurrencyApiAggregator.Tests
{
    public static class TestServiceProviderFactory
    {
        public static IServiceProvider Create()
        {
            return CreateUsingServiceCollection(new ServiceCollection());
        }

        public static IServiceProvider CreateUsingServiceCollection(IServiceCollection services)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            var settingsFileContent = System.IO.File.ReadAllText("testsettings.json");
            var apiSettings = JsonConvert.DeserializeObject<ApiSettings>(settingsFileContent);

            IMemoryCache memoryCache = new MemoryCache(new MemoryCacheOptions());

            services.AddSingleton<IApiClientFactory>(s => new ApiClientFactory(apiSettings));
            services.AddScoped<ICurrencyApiService, CurrencyApiService>();
            services.AddScoped<IMarketDataCacheService>(s => new MarketDataCacheService(
                memoryCache,
                s.GetRequiredService<IApiClientFactory>(),
                TimeSpan.FromSeconds(apiSettings.CachedDataLifetimeInSeconds)));

            return services.BuildServiceProvider(); 
        }
    }
}